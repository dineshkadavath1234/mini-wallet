from django.shortcuts import render
from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from uuid import uuid4
from .models import Customer, Transaction
from django.http import HttpResponse


class CreateAccount(APIView):
	def get(self, request):
		token = request.headers.get('token')
		if token:
			disabled = request.GET.get('is_disabled')
			if disabled:
				entity = Customer.objects.filter(token=token)
				if entity:
					user_id = entity.user_id
					wallet_entity = Wallet.objects.filter(user_id=user_id)
					wallet_entity.update(status=False)
					result = {
					  "status": "success",
					  "data": {
					    "wallet": {
					      "id": wallet_entity.id,
					      "owned_by": user_id,
					      "status": "disabled",
					      "disabled_at": wall_entity.last_updated_time,
					      "balance": wall_entity.balance
					    }
					  }
					}
		else:
			customer_xid = request.GET.get('customer_xid')
			rand_token = uuid4()
			a = Customer(user_id=customer_xid,token=rand_token)
			a.save()

class EnableWallet(APIView):
	def get(self, request):
		token = request.headers.get('token')
		entity = Customer.objects.filter(token=token)
		user_id = entity.user_id
		wallet_entity = Wallet.objects.filter(user_id=user_id)
		if not wallet_entity.status:
			b = Wallet(user_id=user_id,status=True)
			b.save()
			if b:
				result = {
						  "status": "success",
						  "data": {
						    "wallet": {
						      "id": b.id,
						      "owned_by": user_id,
						      "status": "enabled",
						      "enabled_at": b.last_updated_time,
						      "balance": 0
						    }
						  }
						}
		else:
			result = {"status":"Already enabled",
					"data": {
						    "wallet": {
						      "id": wallet_entity.id,
						      "owned_by": user_id,
						      "status": "enabled",
						      "enabled_at": wallet_entity.last_updated_time,
						      "balance": wallet_entity.balance
						    }
						  }}
		return HttpResponse(result)	


class DepositWallet(APIView):
	def post(self, request):
		token = request.GET.get('token')
		amount = request.GET.get('amount')
		if amount!=0:
			cust_entity = Customer.objects.filter(token=token)
			if cust_entity:
				wall_entity = Wallet.objects.filter(user_id=cust_entity.user_id)
				curr_balance = wall_entity.balance
				wal_update = wall_entity.update(balance=curr_balance+amount)
				trans_entity = Transaction.save(amount=amount,user_id=cust_entity.user_id,wallet_id=wall_entity.id)
				if wal_update and trans_entity:
					result = {
							  "status": "success",
							  "data": {
							    "deposit": {
							      "id": wallet_entity.id,
							      "deposited_by": cust_entity.user_id,
							      "status": "success",
							      "deposited_at":trans_entity.transaction_time ,
							      "amount": 0,
							      "reference_id": trans_entity.id
							    }
							}
						}
				else:
					result = { "status": "Failed transaction"}
			else:
				result = { "status": "Failed transaction"}
		else:
			result = { "status": "Failed transaction"}

		return HttpResponse(result)

class WithdrawalWallet(APIView):
	def post(self, request):
		token = request.GET.get('token')
		amount = request.GET.get('amount')
		if amount!=0:
			cust_entity = Customer.objects.filter(token=token)
			if cust_entity:
				wall_entity = Wallet.objects.filter(user_id=cust_entity.user_id)
				curr_balance = wall_entity.balance
				if amount<=curr_balance:
					new_balance = curr_balance-amount
					wal_update = wall_entity.update(balance=new_balance)
					trans_entity = Transaction.save(amount=-(amount),user_id=cust_entity.user_id,wallet_id=wall_entity.id)
					if trans_entity and wal_update:
						result = {
								  "status": "success",
								  "data": {
								    "withdrawal": {
								      "id": wallet_entity.id,
								      "withdrawn_by": cust_entity.user_id,
								      "status": "success",
								      "withdrawn_at": trans_entity.transaction_time,
								      "amount": str(amount),
								      "reference_id": trans_entity.id
								    }
								  }
								}
					else:
						result = { "status": "Failed transaction"}
				else:
					result = { "status": "Failed transaction"}
			else:
				result = { "status": "Failed transaction"}
		else:
			result = { "status": "Failed transaction"}
			
		return HttpResponse(result)






							 


