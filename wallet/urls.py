
from .views import InitializeAccount, CreateAccount
from django.urls import path

urlpatterns = [
	path('api/v1/init',CreateAccount.as_view()),
	path('api/v1/wallet',EnableWallet.as_view()),
	path('api/v1/wallet/deposits',DepositWallet.as_view()),
	path('api/v1/wallet/withdrawals',WithdrawalWallet.as_view())
	
]