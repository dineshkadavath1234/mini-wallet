from django.db import models

# Create your models here.
class Customer(models.Model):
	user_id = models.CharField(primary_key = True ,max_length=20)
	token = models.CharField(max_length=200, default="")

class Transaction(models.Model):
	amount = models.CharField(max_length=10)
	user_id = models.ForeignKey('Customer', on_delete= models.CASCADE, null= True, default= None)
	wallet_id = models.ForeignKey('Wallet', on_delete= models.CASCADE, null= True, default= None)
	transaction_time = models.DateTimeField()

class Wallet(models.Model):
	status = models.BooleanField(default=False)
	user_id = models.ForeignKey('Customer', on_delete= models.CASCADE, null= True, default= None)
	balance = models.IntegerField(default=0)
	last_update_time = models.DateTimeField()