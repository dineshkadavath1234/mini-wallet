# Generated by Django 3.1.7 on 2021-04-08 18:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('user_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('balance', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reference_id', models.CharField(max_length=20)),
                ('amount', models.CharField(max_length=10)),
                ('user_id', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='wallet.customer')),
            ],
        ),
    ]
